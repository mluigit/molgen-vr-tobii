﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Based on
// Alan Zucconi
// www.alanzucconi.com
Shader "heatmap/Heatmap_Push" {
	Properties{
		_HeatTex("Texture", 2D) = "white" {}
		_Radius("Heatmap Radius", Range(0,0.5)) = .1
		_HOpacity("Heatmap Opacity", Range(0,1)) = 1
		_Intensity("Intensity", Range(0.05,3)) = 0
	}

	SubShader{
		Tags{ "Queue" = "Transparent+1"  }
		Blend SrcAlpha OneMinusSrcAlpha // Alpha blend

		Cull Off
		ZWrite Off
		Offset -1, -1

		Pass{
			CGPROGRAM
#pragma vertex vert             
#pragma fragment frag
#pragma target 5.0
#include "UnityCG.cginc"

			struct vertInput {
				float4 pos : POSITION;
				float3 normal : NORMAL;
			};

			struct vertOutput {
				float4 pos : POSITION;
				fixed3 worldPos : TEXCOORD1;
			};

			vertOutput vert(vertInput input) {
				vertOutput o;
				o.pos = UnityObjectToClipPos(input.pos);
				o.worldPos = mul(unity_ObjectToWorld, input.pos).xyz;

				return o;
			}

			uniform int _Points_Length = 0;
			uniform float3 _Points[1000];		// (x, y, z) = position
			uniform float _PointIntensity[1000];

			half _Radius;
			half _HOpacity;
			half _Intensity;
			sampler2D _HeatTex;

			half4 frag(vertOutput output) : COLOR{
				// Loops over all the points
				half h = 0;

				for (int i = 0; i < _Points_Length; i++)
				{
					// Calculates the contribution of each point
					half di = distance(output.worldPos, _Points[i].xyz);
					half hi = 1 - saturate(di / _Radius);
					h += hi * _Intensity * _PointIntensity[i];
				}

				// Converts (0-1) according to the heat texture
				h = saturate(h);
				half4 color = tex2D(_HeatTex, fixed2(h, 0.5));
				color.a *= _HOpacity;
				return color;
			}
			ENDCG
		}
	}

	Fallback "Diffuse"
}