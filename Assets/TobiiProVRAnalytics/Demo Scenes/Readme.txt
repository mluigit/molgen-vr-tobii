//
This file is a short guide to the Demo Unity scenes provided in the Pro VR Analytics package. 
They are meant to help with the Tobii Pro VR Analytics integration by providing some examples that can be used as references or for further development if desired.

There are two scenes provided, both are integrated with all capabilities of Pro VR Analytics but one uses the Pro VR Analytics
Character Controller and the other uses a custom Character Controller. Use the example that matches what kind of character controller you will use in your project.


The scene VR Analytics Character Controller is a fully workable demo scene using the Pro VR Analytics character controller. 


The scene Custom Character Controller is not built to work on its own but rather meant to show how to integrate a character controller from places other than Pro VR Analytics, such as SteamVR plugin. 
A custom character needs to be integrated to be able to play the scene. For instructions, see Pro VR Analytics Integration Manual section 3.1. 

Key things to note for Custom Character Controller:

1. The Participant component needs to be placed on the Camera gameObject rendering to the VR headset. 
	The Spectator camera field in the Participant component is the camera that will render to the monitor during recording.
	If left empty, it will be automatically created based on the VR camera.
2. The Controllers prefab needs to be a child of the root of the VR camera.

//