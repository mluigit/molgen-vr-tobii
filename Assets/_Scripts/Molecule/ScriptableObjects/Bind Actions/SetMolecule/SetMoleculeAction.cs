﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Molecule/Bind Actions/Set Molecule")]
public class SetMoleculeAction : BindAction {

    public Molecule molecule;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        moleculeStateMachine.SetMolecule(molecule);
    }
}
