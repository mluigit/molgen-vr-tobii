﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Auto Bind On Bind")]
public class AutoBindOnBind : BindAction {

    public bool autoBind;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        boundMolecule.AutoBind = autoBind;
    }
}
