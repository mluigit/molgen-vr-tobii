﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Repress")]
public class RepressAction : BindAction {

    public bool repress;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        MoleculeManager.Instance.Repressed = repress;
    }
}
