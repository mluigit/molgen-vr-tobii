﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Destroy Children")]
public class DestroyChildren : BindAction
{

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        foreach(Transform t in moleculeStateMachine.modelParent)
        {
            Destroy(t.gameObject);
        }
    }
}

