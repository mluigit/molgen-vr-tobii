﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Reset Active Sites")]
public class ResetActiveSitesAction : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        moleculeStateMachine.ResetMolecule(moleculeStateMachine.molecule.cooldownTime);
    }
}
