﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Spawn Molecule")]
public class SpawnMolecule : BindAction {

    public Molecule spawnMolecule;
    [Tooltip("Optionally check if correct ligand is bound in cases where more than one substrate per active site is possible")]
    public Molecule ligand;
    public bool spawnAtActiveSite;
    public bool parentToSite;
    public bool parentToLigand;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine molecule, Transform activeSite)
    {
        if (ligand != null && molecule.molecule != ligand)
            return;

        MoleculeManager moleculeManager = FindObjectOfType<MoleculeManager>();
        if(moleculeManager == null)
            return;

        MoleculeStateMachine newMolecule;
        if (spawnAtActiveSite)
            newMolecule = moleculeManager.SpawnMolecule(spawnMolecule, activeSite);
        else
            newMolecule = moleculeManager.SpawnMolecule(spawnMolecule, moleculeStateMachine.transform);

        if (parentToSite)
            newMolecule.transform.SetParent(moleculeStateMachine.transform, false);
        else if(parentToLigand)
        {
            newMolecule.transform.SetParent(molecule.transform, false);
        }
            
        else
        {
            newMolecule.transform.SetParent(null, false);
            newMolecule.GetComponent<Rigidbody>().velocity = moleculeStateMachine.GetComponent<Rigidbody>().velocity;
        }
            
    }
}
