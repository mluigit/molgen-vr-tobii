﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Spawn Continuous")]
public class SpawnContinuous : BindAction {

    public Molecule molecule;
    public float interval;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        MoleculeManager moleculeManager = FindObjectOfType<MoleculeManager>();
        if (moleculeManager == null)
            return;

        moleculeManager.SpawnContinuous(molecule, activeSite, interval);
    }
}
