﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BindAction : ScriptableObject {

    public abstract void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite);
}
