﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Molecule/Bind Actions/Disable Repression")]
public class DisableRepression : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        RepressorController controller = moleculeStateMachine.GetComponentInChildren<RepressorController>();
        if (controller == null)
            return;

        if (controller.boundToOperator)
        {
            MoleculeManager.Instance.ResetOperator();
        }
            
    }
}
