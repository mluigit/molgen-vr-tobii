﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Notify Repressor")]
public class NotifyRepressorController : BindAction {

    public bool bound;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        RepressorController controller = boundMolecule.GetComponentInChildren<RepressorController>();
        if (controller == null)
            return;

        controller.BindToOperator(bound);
    }

}
