﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Register DNA")]
public class RegisterDNAHolder : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        MoleculeManager.Instance.RegisterDNAholder(moleculeStateMachine.molecule);
    }
}
