﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Set to Bound Molecule")]
public class SetToBoundMoleculeAction : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine molecule, Transform activeSite)
    {
        moleculeStateMachine.SetMolecule(molecule.molecule);
    }
}
