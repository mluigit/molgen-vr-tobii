﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class MoleculeButton : UIElement {

    //the molecule prefab to spawn
    public Molecule molecule;

    //molecule management
    private MoleculeManager _molManager;

    //hand attachment
    private Hand _currentHand;

    //cooldown
    private float _timeSinceSpawn;
    private float _cooldown = 0.5f;

    protected override void Awake()
    {
        base.Awake();
        _molManager = FindObjectOfType(typeof(MoleculeManager)) as MoleculeManager;
    }

    //set the hand to attach object to
    protected override void OnHandHoverBegin(Hand hand)
    {
        base.OnHandHoverBegin(hand);
        _currentHand = hand;
    }


    //spawn the object
    public void SpawnMolecule()
    {
        if (_currentHand == null)
            return;

        //prevent spamming
        if (Time.time - _timeSinceSpawn < _cooldown)
            return;

        //have we maxed out this molecule?
        if (_molManager.AtMax(molecule))
            return;

        GameObject prefabObject = Instantiate(_molManager.moleculePrefab) as GameObject;
        prefabObject.GetComponent<MoleculeStateMachine>().SetMolecule(molecule);
        _currentHand.AttachObject(prefabObject, GrabTypes.Scripted);
        _timeSinceSpawn = Time.time;
    }
}
