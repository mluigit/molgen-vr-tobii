﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR.Extras;

[RequireComponent(typeof(Hand))]
public class Pointer : SteamVR_LaserPointer {

    //the hand casting this pointer
    private Hand _hand;
    public Hand Hand { get { return _hand; } }

    //public variable
    public PointerStateMachine pointerStateMachine;

    //private fields
    private Transform _oldHoverSphereTransform;
    public Transform OldHoverSphereTransform { get { return _oldHoverSphereTransform; } }

    private void Awake()
    {
        _hand = GetComponent<Hand>();
        _oldHoverSphereTransform = _hand.hoverSphereTransform;
    }

    //on hover in
    public override void OnPointerIn(PointerEventArgs e)
    {
        base.OnPointerIn(e);

        pointerStateMachine.OnHoverInState(this, e);
    }

    //on hover out
    public override void OnPointerOut(PointerEventArgs e)
    {
        base.OnPointerOut(e);

        pointerStateMachine.OnHoverOutState(this, e);
    }

    //on click
    public override void OnPointerClick(PointerEventArgs e)
    {
        base.OnPointerClick(e);

        pointerStateMachine.OnClickState(this, e);
    }

}
