﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Presets/Pointer/State Preset")]
public class PointerStatePreset : ScriptableObject {

    [Header("States")]
    public State hoverInState;
    public State hoverOutState;
    public State clickState;
}
