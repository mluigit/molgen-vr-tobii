﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Actions/Pointer/Reset Hover Transform")]
public class ResetHoverTransformAction : StateAction
{

    public override void Act(StateMachine stateMachine)
    {
        PointerStateMachine pointerStateMachine = stateMachine as PointerStateMachine;

        if (pointerStateMachine == null)
            return;

        //reset hoversphere transform
        pointerStateMachine.Pointer.Hand.hoverSphereTransform = pointerStateMachine.Pointer.OldHoverSphereTransform;
    }
}
