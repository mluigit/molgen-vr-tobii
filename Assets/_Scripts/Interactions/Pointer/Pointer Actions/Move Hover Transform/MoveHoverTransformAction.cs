﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Actions/Pointer/Move Hover Transform")]
public class MoveHoverTransformAction : StateAction {

    public override void Act(StateMachine stateMachine)
    {
        PointerStateMachine pointerStateMachine = stateMachine as PointerStateMachine;

        if (pointerStateMachine == null)
            return;

        if (pointerStateMachine.PointerEventArgs.distance < pointerStateMachine.Pointer.Hand.hoverSphereRadius)
            return;

        //move hoversphere to interactable object
        pointerStateMachine.Pointer.Hand.hoverSphereTransform = pointerStateMachine.PointerEventArgs.target;
    }
}
