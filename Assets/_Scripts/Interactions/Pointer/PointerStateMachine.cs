﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.Extras;

public class PointerStateMachine : StateMachine {

    //public fields
    public PointerStatePreset pointerPreset;

    public State hoverInState;
    public State hoverOutState;
    public State clickState;

    //properties
    private Pointer _pointer;
    public Pointer Pointer { get { return _pointer; } }

    private PointerEventArgs _pointerEventArgs;
    public PointerEventArgs PointerEventArgs { get { return _pointerEventArgs; } }

    private void Awake()
    {
        //apply preset on wake
        ApplyPreset(pointerPreset);
    }

    //update states based on preset
    public void ApplyPreset(PointerStatePreset preset)
    {
        hoverInState = preset.hoverInState;
        hoverOutState = preset.hoverOutState;
        clickState = preset.clickState;
    }

    //called by pointer
    public void OnHoverInState(Pointer pointer, PointerEventArgs e)
    {
        _pointer = pointer;
        _pointerEventArgs = e;

        if(hoverInState != null)
            ChangeState(hoverInState);
    }

    public void OnHoverOutState(Pointer pointer, PointerEventArgs e)
    {
        _pointer = pointer;
        _pointerEventArgs = e;

        if(hoverOutState != null)
            ChangeState(hoverOutState);
    }

    public void OnClickState(Pointer pointer, PointerEventArgs e)
    {
        _pointer = pointer;
        _pointerEventArgs = e;

        if(clickState != null)
            ChangeState(clickState);
    }
}

