﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    
/// <summary>
/// Subscribes to StateMachineEvents on specificed StateMachine.
/// 
/// </summary>

    public class StateBehaviour : MonoBehaviour
    {
        public StateMachine stateMachine;
        public State[] activeStates; // states to listen for
        private bool inActiveState;
        private int subscriptionCount;
        [SerializeField]
        private bool subscribed;
        public bool disableAutoSubscribing; //disables autosubscribing, must subscribe manually

        // --- STATE MACHIEN EVENT SUBSCRIPTION --- //

        //subscribe to StateMachineEvents when gameobject is enabled
        protected virtual void OnEnable()
        {
            if (!disableAutoSubscribing)
                Subscribe();
        }

        //unsubscribe to StateMachineEvents when gameobject is disabled
        protected virtual void OnDisable()
        {
            Unsubscribe();
        }

        public void Subscribe()
        {
            if (subscriptionCount > 0)
            {
                Unsubscribe();
                Subscribe();
                return;
            }

            if (stateMachine == null)
            {
                Debug.Log("Cannot subscribe to state machine.");
                return;
            }

            stateMachine.onStateEnter += AnyStateEnter;
            stateMachine.onStateExit += AnyStateExit;
            subscriptionCount++;

            subscribed = true;
        }

        public void Unsubscribe()
        {
            if (subscriptionCount < 1)
                return;

            if (stateMachine == null)
                return;
            stateMachine.onStateEnter -= AnyStateEnter;
            stateMachine.onStateExit -= AnyStateExit;
            subscriptionCount--;

            subscribed = false;
        }

        // --- BEHAVIOURS --- //
        
        
        public void AnyStateEnter(StateMachine _stateMachine, State _fromState, State _toState)
        {
            OnAnyStateEnter(_stateMachine, _fromState, _toState);

            if (!IsActiveState(_fromState) && IsActiveState(_toState))
                OnActiveStateEnter(_stateMachine, _toState);
        }

        public void AnyStateExit(StateMachine _stateMachine, State _fromState, State _toState)
        {
            OnAnyStateExit(_stateMachine, _fromState, _toState);

            if (IsActiveState(_fromState) && !IsActiveState(_toState))
                OnActiveStateExit(_stateMachine, _fromState);
        }

        //called when moving from and inactive state to and active state
        public virtual void OnActiveStateEnter(StateMachine _stateMachine, State _state)
        {

        }

        //called when moving from an active state to and inactive state
        public virtual void OnActiveStateExit(StateMachine _stateMachine, State _state)
        {

        }

        //called when any state is entered, whether its listed in the active states or not.
        public virtual void OnAnyStateEnter(StateMachine _stateMachine, State _fromState, State _toState)
        {

        }

        //called when any state is exited, whether its listed in the active states or not.
        public virtual void OnAnyStateExit(StateMachine _stateMachine, State _fromState, State _toState)
        {

        }

        // --- UTILS --- //

        private bool IsActiveState(State _state)
        {
            for (int i = 0; i < activeStates.Length; i++)
            {
                if (activeStates[i] == _state)
                    return true;
            }
            return false;
        }

        public bool IsSubscribed()
        {
            return subscribed;
        }
    }

