﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SampleStateMachine01/Decisions/Mouse Up")]
public class MouseUpDecision : StateDecision {

    public override bool Decide(StateMachine stateMachine)
    {
        //check if mouse button has been released
        if (Input.GetMouseButtonUp(0))
        {
            return true; //change state
        }
            
        return false; //stay in current state
    }
}
