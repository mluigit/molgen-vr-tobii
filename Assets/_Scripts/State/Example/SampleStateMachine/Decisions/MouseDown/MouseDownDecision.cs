﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SampleStateMachine01/Decisions/Mouse Down")]
public class MouseDownDecision : StateDecision {

    public override bool Decide(StateMachine stateMachine)
    {
        //check if StateMachine object has been clicked
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.transform == stateMachine.transform)
                {
                    return true; //change state
                }
            }

            Debug.Log("Mouse down!");
        }


        //stay in current state
        return false;
    }
}
