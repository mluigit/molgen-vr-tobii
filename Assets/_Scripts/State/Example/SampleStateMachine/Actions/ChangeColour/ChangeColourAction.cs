﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SampleStateMachine01/Actions/Change Colour")]
public class ChangeColourAction : StateAction {

    public Color32 color;

    public override void Act(StateMachine stateMachine)
    {
        SampleStateMachine sampleStateMachine = stateMachine as SampleStateMachine;

        if(sampleStateMachine != null)
        {
            sampleStateMachine.GetComponent<Renderer>().sharedMaterial.color = color; //change colour
        }
    }
}
