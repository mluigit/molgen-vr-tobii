﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

/// <summary>
/// 
/// This decision returns the true state when the playable director goes from playing to paused.
/// 
/// This is placed on the Decision field in the Transitions list found on a State scriptable object.
/// When the state machine enters a state with an attached decision, the decision will be evaluated
/// each frame until the conditions return true or the state is changed.
/// If the decision returns true, the True State specified in the Transitions list will be called.
/// 
/// </summary>

[CreateAssetMenu(menuName = "Playable Director State Machine/Decisions/Timeline Complete")]
public class PlayableDirectorOnCompleteDecision : StateDecision {

    private bool hasStarted;
    PlayableDirectorStateMachine directorStateMachine;

    public override bool Decide(StateMachine stateMachine)
    {
        directorStateMachine = stateMachine as PlayableDirectorStateMachine;

        //check to see if current timeline has started playing
        if (!hasStarted)
        {
            hasStarted = directorStateMachine.IsPlaying();
            return false;
        }

        return TimelineDone();
    }

    private bool TimelineDone()
    {
        //return false if the timeline is still playing
        if (directorStateMachine.CurrentPlayState() == PlayState.Playing)
            return false;
         
        return true;
    }
}
