﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName ="Molecule/Anatytics")]
public class LogAnalytics : StateAction {

    public string metric;

    public override void Act(StateMachine stateMachine)
    {
        AnalyticsTracker.Instance.LogItem(DateTime.Now, stateMachine.gameObject.name, metric);
    }
}
