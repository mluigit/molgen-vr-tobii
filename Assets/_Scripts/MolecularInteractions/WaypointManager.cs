﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointManager : MonoBehaviour {

	public enum WaypointGroup { NONE, LAC_I, LAC, LAC_FULL }

    [System.Serializable]
    public class Waypoint
    {
        public WaypointGroup group;
        public Transform[] waypoints;
    }

    public Waypoint[] allWaypoints;

    public Transform[] GetPointsByGroup(WaypointGroup group)
    {
        foreach(Waypoint w in allWaypoints)
        {
            if (w.group == group)
                return w.waypoints;
        }
        return null;
    }
}
