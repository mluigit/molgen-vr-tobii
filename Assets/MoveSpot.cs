﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpot : MonoBehaviour {
    public Transform spot1;
    public Transform spot2;
    private Transform _transform;
    float time = 0.0f;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (time > 3.0f)
        {
            transform.position = spot1.position;
        }
        if (time > 6.0f)
        {
            transform.position = spot2.position;
            time = 0.0f;

        }
		
	}
}
