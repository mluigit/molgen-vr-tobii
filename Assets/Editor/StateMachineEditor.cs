﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(StateMachine),true)]
public class StateMachineEditor : Editor {

    private ReorderableList list;
    private SerializedProperty currentState;

    private void OnEnable()
    {
        //current state
        currentState = serializedObject.FindProperty("currentState");

        //state order
        list = new ReorderableList(serializedObject, serializedObject.FindProperty("stateOrder"), true, true, true, true);
        list.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = list.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2;
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight),
                    element, GUIContent.none
                    );
            };

        list.drawHeaderCallback = (Rect rect) => {
            EditorGUI.LabelField(rect, "State Order");
        };
    }


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space();
        serializedObject.Update();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();

        StateMachine stateMachine = (StateMachine)target;

        EditorGUILayout.Space();
        if (GUILayout.Button("Next State"))
        {
            stateMachine.NextState();
        }
        if (GUILayout.Button("Previous State"))
        {
            stateMachine.PrevState();
        }
    }
}
