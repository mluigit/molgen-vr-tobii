﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LactoseGraphInformer : MonoBehaviour {

    private GameObject graph;
    // Use this for initialization
    void Start()
    {
        graph = GameObject.FindGameObjectWithTag("graph");
        graph.GetComponent<BacteriaGraph>().AddLactose(+1);
    }

    private void OnDestroy()
    {
        graph.GetComponent<BacteriaGraph>().AddLactose(-1);
    }
}
