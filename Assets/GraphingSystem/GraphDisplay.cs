﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class GraphDisplay : MonoBehaviour {

    public SteamVR_Action_Boolean action = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("default", "Teleport");
    private Interactable interactable;
    SteamVR_Input_Sources hand;
    private bool isActive;
    private bool isPinching;
    // Use this for initialization
    void Start () {
        // interactable = GetComponent<Interactable>();
        isActive = false;
        hand = Object.FindObjectOfType<Hand>().handType;

    }
	
	// Update is called once per frame
	void Update () {
        hand = Object.FindObjectOfType<Hand>().handType;
        isPinching = action.GetState(hand);
        if (isPinching && !isActive)
        {
            this.transform.GetChild(0).gameObject.GetComponent<Canvas>().enabled = true;
            isActive = true;
        }
        else if (isPinching && isActive)
        {
            this.transform.GetChild(0).gameObject.GetComponent<Canvas>().enabled = false; 
            isActive = false;
        }
    }
}
