﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneSwitchButton : MonoBehaviour
{
    public string otherScene;
    // Start is called before the first frame update
    void SwitchScene()
    {
        GameObject levelManager = GameObject.FindGameObjectWithTag("LevelManager");
        levelManager.GetComponent<LevelManager>().LoadScene(otherScene);
    }

    // Update is called once per frame
    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(SwitchScene);

    }
}
