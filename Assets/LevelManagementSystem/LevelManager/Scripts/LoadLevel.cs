﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevel : MonoBehaviour {

	public void LoadLevelScene(string scene)
    {
        LevelManager.instance.LoadScene(scene);
    }
}
